let nombre = 'Peter Parcker';
console.log(nombre);

//aqui estamos cambiando el contenido a donde apunta nombre
nombre = 'Petra Parker';
console.log(nombre);

console.log(typeof nombre);

let esMarvel = false;
console.log(typeof esMarvel);

let edad = 33;
console.log(typeof edad);

// Se Recomienda el camel case
let superPoder;
console.log(typeof superPoder); //??

let soyNull = null;
console.log(typeof soyNull);

let symbol1 = Symbol();
let symbol2 = Symbol('a');

console.log(typeof symbol1);

console.log(symbol1 === symbol2);