// Funciones Tradicionales, las funciones siempre retonan en caso de no colocar
// explicitamente lo que retorna, lo hara en undefined
function saludar(nombre) {
    // console.log('Hola Mundo ' + nombre);
    // console.log(arguments);

    return [1, 2, 3, 4, 5];

    // el codigo despues del retorn nunca se va a ejecutar
    console.log('Texto Prueba');
}



// Funciones Anonimas en una constante. ya que asi no se puede reutilizar //
//el nombre saludar2
const saludar2 = function(parametro) {
    console.log('Hola Mundo desde funcion Anonima ' + parametro);
}



// Funciones de Flecha, //
// los () son opcinales pero es buena pract dejarlas
const saludarFlecha = (nombre) => {
    console.log('Hola desde Flecha ' + nombre);
}

// Argumentos son los parametros que le enviamos a la funcion
// cuando una variable no esta inicializada es undefined

const valorRetorno = saludar('Elio', 40, 50);
// console.log(valorRetorno[0], valorRetorno[1]);

// saludar2('Practica');
// saludarFlecha('Campeon');


// ¿Para que me Sirve el retorn ? //

function sumar(a, b) {
    return a + b;
}

// lo hacemos asi cuando necesitamos retornar mas cosas
const sumarFlecha = (a, b) => {
    return a + b;
}

// las funciones de flecha cuando tiene un solo return la podemos resumir asi:
const retornUnValor = (a, b) => a * b;

function getAleatorio() {
    return Math.random();
}

// Ejercicio de usar una fun de flecha que no tenga llaves
// getAleatorio2

const getAleatorio2 = () => Math.random();



// console.log(sumar(20, 6));
// console.log(sumarFlecha(20, 6));
// console.log(retornUnValor(20, 6));

console.log(getAleatorio2());