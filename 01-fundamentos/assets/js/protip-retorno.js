// Funciones Pro ;) Aprenderlo
// Funcion Anonima
// function createPersona(nombre, apellido) {
//     return { nombre, apellido }
// }

// Funcion de Flecha con Protips ;) Aprenderlo
const createPersona = (nombre, apellido) => ({ nombre, apellido });

const persona = createPersona('Elio', 'Alfonzo');
console.log(persona);

// Funcion Tradicional con Pro Tips   ;)
// Las Func Tradicionales si reconocen a arguments pero las de flechas no
// Solucionamos eso de esta manera (args) para que reciba un solo valor
// (...args) para recibir varios valores


function imprimrArgumentos() {
    console.log('Func Tradicional', arguments);
}

imprimrArgumentos(12, 'Elio', 20796387);

// Si queremos recibir un solo valor
const imprimrArgumentos2 = (args) => {
    console.log(args);
};

imprimrArgumentos2(12, 'Elio', 20796387);

// Si queremos recibir varios Valores con rest
const imprimrArgumentos3 = (...args) => {
    console.log(args);
};

imprimrArgumentos3(12, 'Elio', 20796387);

// const imprimrArgumentos4 = (edad, ...args) => {
//     // console.log({ edad, args });
//     return ({ edad, args })
// };

// Si queremos tomar un valor antes de recibir varios Valores con rest
const imprimrArgumentos4 = (...args) => {
    // console.log({ edad, args });
    return (args)
};

const argumentos = imprimrArgumentos4(12, 'Elio', 20796387, true, 35);
console.log(argumentos);

// si queremos todos esos parametros que se asignes a unas variables o parede valores
const [edad, nombre, cedula, vivo, estatura] = imprimrArgumentos4(12, 'Elio', 20796387, true, 35);
console.log({ edad, nombre, cedula, vivo, estatura });


const { apellido } = createPersona('Elio', 'Sanchez');
console.log({ apellido });

// si queremos que sea otro nombre o sarlo en otra variable
const { apellido: nuevoApellido } = createPersona('Elio', 'Sanchez');
console.log({ nuevoApellido });

// Otro Pro Tip Poderoso

const personaje = {
    nombre: 'Tony Hanks',
    codeName: 'Ironman',
    vivo: false,
    edad: 40,
    trajes: ['Mark 1', 'Mark2', 'Mark3'],
}

//Funciona pero es mucho Trabajo

// const imprimirPropiedades = (persona) => {
//     console.log('Nombre: ', persona.nombre);
//     console.log('Code: ', persona.codeName);
//     console.log('Vivo: ', persona.vivo);
//     console.log('Edad: ', persona.edad);
//     console.log('Trajes: ', persona.trajes);
// }

//Desestructuracion de argumentos y podemos colocarle valores por defecto
//por si vienen vacios
const imprimirPropiedades = ({ nombre, codeName, vivo, edad = 15, trajes }) => {

    console.log(nombre);
    console.log(apellido);
    console.log(codeName);
    console.log(edad);
    console.log(trajes);
}

imprimirPropiedades(personaje);