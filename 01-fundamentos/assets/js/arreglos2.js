let juegos = ['Zelda', 'Mario', 'Kontra', 'PES2020'];
console.log('Largo:', juegos.length);

// esto sirve para array que crecen en el tiempo y dinamicos
let primero = juegos[2 - 2];
let ultimo = juegos[juegos.length - 1];
console.log({ primero, ultimo });

//Metodo Foreach para barrer todo el array con foreach y func de flecha.
juegos.forEach((elemento, indice, arr) => {
    console.log({ elemento, indice, arr });
});


// Metodo para insertar un elemento push
let nuevaLongitud = juegos.push('F-Zero');
console.log(nuevaLongitud, juegos);

// Methods para agregarlo al principio del arreglo unshift()
juegos.unshift('NuevoJuego');
console.log(nuevaLongitud, juegos);

// Method para eliminar el ultimo elemento del array y regresa elemento borrado
let juegoBorrado = juegos.pop();
console.log({ juegoBorrado, juegos });

let pos = 1;
console.log(juegos);

let juegosBorrados = juegos.splice(pos, 2);
console.log({ juegosBorrados, juegos });

//Metodo para buscar un valor en un array, si regresa -1 es que no lo encontro
// y esto es casesensitive
let kontraidIndex = juegos.indexOf('Kontra');
console.log('Kontra: ' + kontraidIndex)