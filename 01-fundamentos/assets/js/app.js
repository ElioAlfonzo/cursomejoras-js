// var b = 10;
// const c = 10;
// Diferencias entre let y var es recomendable 
// siempre crear la variable con let o var
// la inicializacion de var es antigua y ya no se usa
alert('Hola desde app.js');
let a = 10;
let d = 20;
let e = 14;
let f = 20,
    h = 'hola',
    z = 'Querida',
    i = 22,
    j = d + e;

const saludo = h + z
console.log(saludo);

console.log({ j });
// sirve para ver que valor y variable tienen en este momento
console.table({ d, e, f, h })
z = 'como estas';

let outerWidth = 1000000;
const outerHeight = 700;