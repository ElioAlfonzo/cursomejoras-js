// Forma tradicional y forma de inicializar un arreglo de 10 eleentos pero vacio
// let arr = new Array(10);
// let arreglo = [];

let videoJuegos = ['Mario', 'Sonic', 'Megaman'];

console.log(videoJuegos[2]);

let arregloCosas = [
    true,
    123,
    'Hola a todos',
    3 * 3,
    function() {},
    () => {},
    { a: 1 },
    ['X', 'Megaman', 'Zero', 'Dr. Light', [
        'Pedro', 'Pablo'
    ]]
];

// console.log({ arregloCosas });
// console.log(arregloCosas[2]);
console.log(arregloCosas[7][3]);
console.log(arregloCosas[7][4][1]);

let arreglo = arregloCosas[7];
console.log(arreglo[arreglo.length - 1]);