// los objetos literales son los siguente y estan formado por clave : valor
// que a su ves es un variables de un tipo objeto literal por asi decirlo
const personaje = {
    nombre: 'Tony Hanks',
    codeName: 'Ironman',
    vivo: false,
    edad: 40,
    coords: {
        lat: 34,
        lng: -118.70
    },
    trajes: ['Mark 1', 'Mark2', 'Mark3'],
    direccion: {
        zip: '10880, 90265',
        ubicacion: 'Malibu, California'
    },
    'ultima-pelea': 'El Final de Todo'

}

console.log(personaje);
console.log('Nombre:', personaje.nombre);
console.log('Nombre:', personaje['nombre']); //Notacin de Corchetes

console.log('Edad:', personaje.edad);

console.log('Coordenadas:', personaje.coords);
console.log('Latitud:', personaje.coords.lat);
console.log('Longitud:', personaje.coords.lng);

// Numeros de Trajes
console.log('N° de Trajes:', personaje.trajes.length);
// Ultimo Traje
console.log('Ultimo Traje:', personaje.trajes[personaje.trajes.length - 1]);

// Se recomienda usar const para trabajar de forma dinamica con los objetos
// a estas constante la igualamos a alguna propiedad de nuestro objeto para 
// usar estas constantes y acceder facilmente
const x = 'vivo';
console.log('Vivo:', personaje[x]);
console.log('Ultima Pelea:', personaje["ultima-pelea"]);

// Eliminar propiedades de un objeto
delete personaje.edad;
console.log(personaje);

// Anexamos una nueva popiedad
personaje.casado = true;

// Trabajar el objeto en pares Valores 0 y 1
const entriesPares = Object.entries(personaje);
console.log(entriesPares);

// En caso de que no queramos quenuestro objeto no sea cambiado por nada
// lo pasamos a un const y asi no podremos usar personaje en otra cosa
// pero si se pueden editar sus variables

// Bloqueamos cualquier creacion de nuestro objeto personaje
Object.freeze(personaje);
personaje.dinero = 10000;

console.log(personaje);

// Traemos todas las propiedades del obj personaje
const propiedades = Object.getOwnPropertyNames(personaje);
console.log(propiedades);

// Traemos todas los valores del obj personaje
const valores = Object.values(personaje);
console.log('Valores:', valores);